# evaluation_poo_01


#Cration des classes
    1. abstract class Animal qui a les propriétés (nom, categorie, race) et les méthodes(faireLeShow,donnerNaissance);
    2. class Herbivore et Class Carnivore qui sont les enfants de class Animal;
    3. class Personne qui a le propriété(nom) et le méthode (observe)
#Initialisation de Class Zoo
    Dans un zoo, nous avons besoin
    1.  les propriétés: liste des animaux et liste des visiteurs, 
        ces propriétés sont les tableaux des objets, donc, on les initialise en array dans le construct;
    2.  méthode vendreBillet() pour ajouter les visiteurs dans la liste des visiteurs;
    3.  méthode livraison() pour ajouter les animaux dans la liste des animaux;
    4.  méthode ouvrirLesPortes() assure que 
            chaque visiteur fait le parcour complet (les animaux font le show, un par un);
    ...